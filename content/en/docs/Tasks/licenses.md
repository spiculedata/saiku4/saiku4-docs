---
title: "Licenses"
linkTitle: "Licenses"
weight: 2
date: 2019-07-19
description: >
  How create a license for the Saiku platform.
---

Saiku Analytics uses a license model to allow us to enable features and functionality for different users
on the platform. This means we can ship 1 version of Saiku to all our users and ensure that everyone gets the best
 platform we can build. To generate a license you need to visit....
