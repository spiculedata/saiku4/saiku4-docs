---
title: "User Administration"
linkTitle: "User Administration"
weight: 3
date: 2017-01-04
description: >
  How to administer users on the Saiku Server.
---

## User Administration

Firstly you'll see the User Management section. This is where users who are using the internal database can manage users from.

To add a new user, click the Add User button in the top right of the screen. You'll be presented with a popup window that has a form inside.

Username, email, password are obvious enough. Roles is a freeform list of roles that the user has. The admin for example has ROLE_ADMIN, but
normal users might have ROLE_USER, ROLE_UK, ROLE_US etc. This allows you to apply permissions to data sources, row level data and to files and folders
saved within the platform.

Of course you can also Edit existing users by clicking on the pencil icon and delete users by clicking on the Bin icon.

![User Add](/tutorials/tasks/useradd.png)
