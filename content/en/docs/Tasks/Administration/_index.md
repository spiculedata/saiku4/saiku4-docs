---
title: "Administration"
linkTitle: "Administration"
weight: 3
date: 2017-01-04
description: >
  How to administer the Saiku Server.
---


To administer Saiku, first you must login as an Admin user. The default admin user on the platform is admin/admin.

Once you've logged in, click on the console icon on the left side of the screen and you'll be presented with the Administration Console.

From here you have a number of options:

* User Management
* Data Source Management
* License Management

