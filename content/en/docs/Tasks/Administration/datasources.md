---
title: "Data Source Management"
linkTitle: "Data Source Management"
weight: 3
date: 2017-01-04
description: >
  How to administer Saiku data sources.
---

Data sources inside of Saiku require 2 components. A database, and a Mondrian schema.

The database can be almost any JDBC compliant datasource. The schema is an XML file that describes the layout of the OLAP schema that
sits on top of the tables.

![User Add](/tutorials/tasks/datasourceadd.png)
