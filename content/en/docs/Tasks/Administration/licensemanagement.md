---
title: "License Management"
linkTitle: "License Management"
weight: 3
date: 2017-01-04
description: >
  How to manage licenses within Saiku.
---

Saiku has a license engine built into its distribution and this allows us to expose features to users who pay for Enterprise support.
To login you need to have a license installed but you may also want to update it, change the license type or view the info stored within it.

To do this click on the License Management panel and you will see a view like this:

![License](/tutorials/tasks/licenseinfo.png)
