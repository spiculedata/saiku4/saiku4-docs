---
title: "Docker"
linkTitle: "Docker"
weight: 1
date: 2019-07-19
description: >
  Installing using Docker.
---

Getting started with our all in one Docker image is the quickest way to get up and running for most users.

To launch the image is as simple as:

docker run -v /storage:/tmp -p 80:80 registry.gitlab.com/spiculedata/saiku4/saiku4-docker

Then visit http://localhost in your browser.
