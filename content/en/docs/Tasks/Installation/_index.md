---
title: "Installation"
linkTitle: "Installation"
weight: 1
date: 2019-07-19
description: >
  How to install the Saiku Server.
---

There are a range of different ways to install Saiku and this list is growing over time.

Here are some of the more popular installation methods.
