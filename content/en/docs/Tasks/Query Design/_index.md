---
title: "Query Design"
linkTitle: "Query Design"
weight: 5
date: 2019-07-19
description: >
  How to generate queries using Saiku.
---

Designing a query in Saiku is pretty easy to get going, but eventually you want to know more. In this section we'll look 
at the different aspects of query design.
