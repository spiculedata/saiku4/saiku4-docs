
---
title: "Documentation"
linkTitle: "Documentation"
weight: 20
menu:
  main:
    weight: 20
---


Welcome to the Saiku Documentation. 

This guide will help you on your journey with Saiku Analytics. We cover how to install, get started, design schema, write queries and embed Saiku into your
own application.

If you think the documentation is missing a topic please feel free to raise an issue or submit a Merge Request on our documentation repo.
